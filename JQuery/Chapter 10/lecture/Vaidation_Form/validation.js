$(document).ready(function () {
    $(":radio").change(
        function () {
            var radioButton = $(":radio:checked").val();
            if (radioButton == "corporate") {
                $("#company_name").attr("disabled", false);
                $("#company_name").next().text("*");
            } else {
                $("#company_name").attr("disabled", true);
                $("#company_name").next().text("");
            }
        } // end function
    ); // end change

    $("#member_form").submit(
        function (event) {
            var isValid = true;

            // validate the email entry with a regular expression
            var emailPattern =
                /\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b/;
            var email = $("#email").val().trim();
            if (email == "") {
                $("#email").next().text("This field is required.");
                isValid = false;
            } else if (!emailPattern.test(email)) {
                $("#email").next().text("Must be a valid email address.");
                isValid = false;
            } else {
                $("#email").next().text("");
            }
            $("#email").val(email);

            // validate the password entry
            var password = $("#password").val().trim();
            if (password.length < 6) {
                $("#password").next().text("Must be 6 or more characters.");
                isValid = false;
            } else {
                $("#password").next().text("");
            }
            $("#password").val(password);
            ...
            // validate the company name entry
            if (!$("#company_name").attr("disabled")) {
                var companyName = $("#company_name").val().trim();
                if (companyName == "") {
                    $("#company_name").next().text(
                        "This field is required.");
                    isValid = false;
                } else {
                    $("#company_name").next().text("");
                }
                $("#company_name").val(companyName);
            }
            ...
            // prevent the submission of the form if any entries are invalid 
            if (isValid == false) {
                event.preventDefault();
            }
        } // end function
    ); // end submit
}); // end ready
