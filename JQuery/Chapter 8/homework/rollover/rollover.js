"use strict";
$(document).ready(function () {
    $("#image_rollovers img").each(function () {
        var urlOne = $(this).attr("src");
        var urlTwo = $(this).attr("id");

        var image = new Image();
        image.src = urlTwo;
        $(this).mouseover(function () {
            $(this).attr("src", urlTwo);
        });

        $(this).mouseout(function () {
            $(this).attr("src", urlOne);
        });

    }); // end each
}); // end ready
