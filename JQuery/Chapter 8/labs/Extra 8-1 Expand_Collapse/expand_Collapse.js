$(document).ready(function () {
    $("#expandParaOne").hide();
    $("#expandParaThree").hide();

    $("#showMoreOne").click(function () {
        $("#expandParaOne").toggleClass("expand");
        if ($("#expandParaOne").attr("class") != "expand") {
            $("#expandParaOne").hide();
            $("#showMoreOne").text("Show More");
        } else {
            $("#expandParaOne").show();
            $("#showMoreOne").text("Show Less");
        }
    });
    $("#showMoreTwo").click(function () {
        $("#expandParaTwo").toggleClass("expand");
        if ($("#expandParaTwo").attr("class") != "expand") {
            $("#expandParaTwo").hide();
            $("#showMoreTwo").text("Show More");
        } else {
            $("#expandParaTwo").show();
            $("#showMoreTwo").text("Show Less");
        }
    });
    $("#showMoreThree").click(function () {
        $("#expandParaThree").toggleClass("expand");
        if ($("#expandParaThree").attr("class") != "expand") {
            $("#expandParaThree").hide();
            $("#showMoreThree").text("Show More");
        } else {
            $("#expandParaThree").show();
            $("#showMoreThree").text("Show Less");
        }
    });
});
