$(document).ready(function () {
    $("h2").click(
        function () {
            $(this).toggleClass("minus");
            if ($(this).attr("class") !== "minus") {
                $(this).next().hide();
            } else {
                $(this).next().show();
            }

            $("#book_Images").attr("src", "");
        }
    ); // end click

    $("#openSource a, #java a, #net a, #database a").each(function () {
        var hrefLink = $(this).attr("href");

        var image = new Image();
        image.src = hrefLink;

        $(this).click(function (evt) {
            $("#book_Images").attr("src", hrefLink);

            evt.preventDefault();
        }); // end click 
    }); // end each
}); // end ready
