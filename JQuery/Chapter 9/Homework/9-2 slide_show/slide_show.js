$(document).ready(function () {
    var slide = $("#slides img:first-child");
    var caption;
    var src;

    intervalTimer = setInterval(
        function () {
            $("#caption").hide(1000);
            $("#slide").slideUp(2000, function () {
                if (slide.next().length == 0) {
                    slide = $("#slides img:first-child");
                } else {

                    slide = slide.next();
                }
                src = slide.attr("src");
                caption = slide.attr("alt");
                $("#caption").text(caption).show(1000);
                $("#slide").attr("src", src).slideDown(2000);
            }); // end callback
        }, 5000
    );
}); // end ready
