$(document).ready(function () {
    $("h2").click(function () {
        $(this).toggleClass("minus");
        if ($(this).attr("class") != "minus") {
            $(this).next().slideUp(1000, "easeInBounce");
        } else {
            $(this).next().slideDown(1000, "easeOutBounce");
        }
    }); // end click

    $("h1").animate({
            fontSize: "300%",
            opacity: 1,
            left: "+=375"
        }, 2000, "easeInExpo")
        .animate({
            fontSize: "175%",
            left: "-=200"
        }, 2000, "easeOutExpo");

    $("h1").click(function () {
        $(this).animate({
                fontSize: "300%",
                opacity: 1,
                left: "+=375"
            }, 2000, "easeInExpo")
            .animate({
                fontSize: "175%",
                left: 0
            }, 1000, "easeOutExpo");
    }); // end click

}); // end ready
