$(document).ready(function () {

    var slide = $("#image_list");
    var newLeft;
    var oldLeft;

    $("#right_button").click(function () {

        oldLeft = parseInt(slide.css("left"));

        if (oldLeft - 100 <= -900) {
            newLeft = 0;
        } else {
            newLeft = oldLeft - 100;
        }

        slide.animate({
            left: newLeft
        }, 1000);

    });

    $("#left_button").click(function () {


        oldLeft = parseInt(slide.css("left"));


        if (oldLeft < 0) {
            newLeft = oldLeft + 100;
        } else {
            newLeft = -800;
        }


        slide.animate({
            left: newLeft
        }, 1000);

    }); // end click		
}); // end ready
