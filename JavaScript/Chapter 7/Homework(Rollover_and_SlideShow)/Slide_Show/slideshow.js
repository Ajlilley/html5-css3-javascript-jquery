"use strict";
var $ = function (id) {
    return document.getElementById(id);
};

var imageCache = [];
var imageCounter = 0;
var timer;
var runSlideShow = function () {
    imageCounter = (imageCounter + 1) % imageCache.length;
    var image = imageCache[imageCounter]; //this is saying look at the INDEX, of the array "imageCache", thats value is equal to the value of imageCounter; grab the ELEMENT that rests at that index, and set the variable "image" equal to that ELEMENT."
    $("image").src = image.src;
    $("caption").firstChild.nodeValue = image.title;
};

window.onload = function () {
    var listNode = $("image_list"); // the ul element
    var links = listNode.getElementsByTagName("a");

    // Preload images, copy title properties, and store in array
    var i, link, image;
    for (i = 0; i < links.length; i++) {
        link = links[i]; //ask Mr. G about what this does. (Does this tell the program to look at the index value that matches the value of i in the array "links" and sets the variable "link" equal to the ELEMENT in array "links" that is found at that index value?)
        image = new Image(); //sets the variable image to a new Image element/Object aka <img src="" title=""> (new Image() is a premade global object inside of JavaScript)
        image.src = link.getAttribute("href");
        image.title = link.getAttribute("title");
        imageCache[imageCache.length] = image;
    }

    // Attach start and pause event handlers
    $("start").onclick = function () {
        // disable start button, enable pause button,
        // run slide show, and start timer to change 
        // slide every 2 seconds
        runSlideShow();
        timer = setInterval(runSlideShow, 2000);
        $("start").setAttribute("disabled", "true");
        $("pause").removeAttribute("disabled");
    };
    $("pause").onclick = function () {
        // cancel timer, disable pause button,
        // and enable start button
        clearInterval(timer);
        $("start").removeAttribute("disabled");
        $("pause").setAttribute("disabled", "true");
    };
};
