"use strict";
var $ = function (id) {
    return document.getElementById(id);
};

window.onload = runClock();

setInterval(runClock, 1000);

function runClock() {
    var clock = new Date();
    var hours;
    var minutes;
    var seconds;
    var time;
    var am_pm;

    hours = clock.getHours();
    if (hours <= 12) {
        am_pm = " AM"
    } else if (hours > 12) {
        hours = clock.getHours() - 12;
        am_pm = " PM"
    }

    minutes = clock.getMinutes();
    if (minutes < 10) {
        minutes = "0" + minutes;
    } else if (minutes > 9) {
        minutes = clock.getMinutes();
    }

    seconds = clock.getSeconds();
    if (seconds < 10) {
        seconds = "0" + seconds;
    } else if (seconds > 9) {
        seconds = clock.getSeconds();
    }

    time = hours + ":" + minutes + ":" + seconds + am_pm;
    document.getElementById("time").firstChild.nodeValue = time;
};


var start = document.getElementById('start'),
    stop = document.getElementById('stop'),
    reset = document.getElementById('reset'),
    seconds_watch = 0,
    minutes_watch = 0,
    hours_watch = 0,
    time_watch,
    watch;

if (minutes_watch < 10) {
    minutes_watch = "0" + minutes_watch;
}

if (hours_watch < 10) {
    hours_watch = "0" + hours_watch;
}

function addTime() {
    seconds_watch++;
    if (seconds_watch <= 9) {
        seconds_watch = "0" + seconds_watch;
    }
    if (seconds_watch >= 60) {
        seconds_watch = "0" + 0;
        minutes_watch++;
        if (minutes_watch < 10) {
            minutes_watch = "0" + minutes_watch;
        }
    }

    if (minutes_watch >= 60) {
        minutes_watch = "0" + 0;
        seconds_watch = "0" + 0;
        hours_watch++;
        if (hours_watch < 10) {
            hours_watch = "0" + hours_watch;
        }
    };

    $("stop_watch_timer").firstChild.nodeValue = hours_watch + ":" + minutes_watch + ":" + seconds_watch;
};

function timer() {
    console.log(time_watch = setInterval(addTime, 1000));
};

start.onclick = timer;

stop.onclick = function () {
    clearInterval(time_watch);
};

reset.onclick = function () {
    clearInterval(time_watch);
    $("stop_watch_timer").firstChild.nodeValue = "00:00:00";
    seconds_watch = "0" + 0;
    minutes_watch = "0" + 0;
    hours_watch = "0" + 0;
};
