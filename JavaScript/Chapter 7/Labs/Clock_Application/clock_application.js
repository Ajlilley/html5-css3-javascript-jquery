window.onload = runClock();

setInterval(runClock, 1000);

function runClock() {
    var clock = new Date();
    var hours;
    var minutes;
    var seconds;
    var time;
    var am_pm;

    hours = clock.getHours();
    if (hours <= 12) {
        am_pm = " AM"
    } else if (hours > 12) {
        clock.getHours() - 12;
        am_pm = " PM"
    }

    minutes = clock.getMinutes();
    if (minutes < 10) {
        minutes = "0" + minutes;
    } else if (minutes > 9) {
        minutes = clock.getMinutes();
    }

    seconds = clock.getSeconds();
    if (seconds < 10) {
        seconds = "0" + seconds;
    } else if (seconds > 9) {
        seconds = clock.getSeconds();
    }

    time = hours + ":" + minutes + ":" + seconds + am_pm;
    document.getElementById("time").firstChild.nodeValue = time;
};
