var $ = function (id) {
    return document.getElementById(id);
};

function clickFunc() {
    if ($("fahToCel").checked == true) {
        document.getElementById("given_degrees_lbl").firstChild.nodeValue = "Enter F degrees:";
        document.getElementById("converted_degrees_lbl").firstChild.nodeValue = "Degrees Celsius:";
        document.getElementById("given_degrees").value = "";
        document.getElementById("converted_degrees").value = "";

    } else if ($("celToFah").checked == true) {
        document.getElementById("given_degrees_lbl").firstChild.nodeValue = "Enter C degrees:";
        document.getElementById("converted_degrees_lbl").firstChild.nodeValue = "Degrees Fahrenheit:";
        document.getElementById("given_degrees").value = "";
        document.getElementById("converted_degrees").value = "";

    }
};

function convertBtn() {
    if ($("fahToCel").checked == true) {
        var fTempVal = parseFloat(document.getElementById('given_degrees').value);
        var cTempVal = (fTempVal - 32) * (5 / 9);
        document.getElementById('converted_degrees').value = cTempVal.toFixed(2);
        return false;
    } else if ($("celToFah").checked == true) {
        var cTempVal = parseFloat(document.getElementById('given_degrees').value);
        var fTempVal = (cTempVal * (9 / 5)) + 32;
        document.getElementById('converted_degrees').value = fTempVal.toFixed(2);
        return false;
    }
};
