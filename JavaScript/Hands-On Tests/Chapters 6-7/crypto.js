"use strict";
var $ = function (id) {
    return document.getElementById(id);
};

function billing_fill() {

    var billling = $("billing_auto_fill");

    if (billling.checked == true) {
        $("billing_first_name_text").value = $("customer_first_name_text").value;
        $("billing_last_name_text").value = $("customer_last_name_text").value;
        $("billing_street_address_text").value = $("customer_street_address_text").value;
        $("billing_zip_text").value = $("customer_zip_text").value;
        $("billing_phone_text").value = $("customer_phone_text").value;
    }
    else if (billling.checked == false) {
        $("billing_first_name_text").value = $("billing_first_name_text").value;
        $("billing_last_name_text").value = $("billing_last_name_text").value;
        $("billing_last_name_text").value = $("billing_street_address_text").value;
        $("billing_last_name_text").value = $("billing_zip_text").value;
        $("billing_last_name_text").value = $("billing_phone_text").value;
    }
}

function ValidationEvent() {
    
    var bitcoin_currency = document.getElementById("bitcoin_currency");
    var ethereum_currency = document.getElementById("ethereum_currency");
    var hardware_wallet = document.getElementById("hardware_wallet");
    var customer_first_name = document.getElementById("customer_first_name_text").value;
    var customer_last_name = document.getElementById("customer_last_name_text").value;
    var customer_email = document.getElementById("customer_email_text").value;

    if (bitcoin_currency.checked == true || ethereum_currency.checked == true) {
        return true;
    }
    else {
        document.getElementsByTagName("validation_currency").removeAttribute("hidden");
        return false;
    }
}