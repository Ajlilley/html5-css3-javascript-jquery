var sales;

var $ = function (id) {
    'use strict';
    return document.getElementById(id);
};

var calculateSales = function (subtotal, rate) {
    'use strict';
    sales = subtotal;
    sales = sales * rate / 100;
    return sales;
};

var calculateTotal = function (subtotal, rate) {
    'use strict';
    var total = subtotal + sales;
    return total;
};

var processEntries = function () {
    'use strict';
    var subtotal = parseFloat($("subTotal").value);
    var rate = parseFloat($("rate").value);
    $("sales").value = calculateSales(subtotal, rate);
    $("total").value = calculateTotal(subtotal, rate);
};

window.onload = function () {
    'use strict';
    $("calculate").onclick = processEntries;
};
